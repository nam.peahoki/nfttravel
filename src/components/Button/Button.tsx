import React, { FC } from 'react'
import { Button as AntButton, ButtonProps } from 'antd'
import clsx from 'clsx'
import s from './Button.module.scss'

const Button: FC<ButtonProps> = ({ className, children, ...rest }) => (
  <AntButton
    className={clsx({
      [s.root]: true,
      [className || '']: true,
    })}
    {...rest}
  >
    {children}
  </AntButton>
)

export default Button
