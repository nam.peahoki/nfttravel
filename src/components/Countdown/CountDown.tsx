import React from 'react'
import Countdown, { CountdownRenderProps } from 'react-countdown'
import s from './CountDown.module.scss'

const CountDown = () => {
  const Completionist = () => <span>Finish!!!</span>
  const renderer = ({ days, hours, minutes, seconds, completed }: CountdownRenderProps) => {
    if (completed) {
      return <Completionist />
    }
    return (
      <span className={s.countdown}>
        {days}D:{hours}H:{minutes}M:{seconds}S
      </span>
    )
  }
  return <Countdown className={s.countdown} date={Date.now() + 10000000000} renderer={renderer} />
}
export default CountDown
