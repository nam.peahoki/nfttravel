import { Avatar, Card as AntdCard, CardProps as AntdCardProps, Space } from 'antd'
import { FC, ReactNode } from 'react'
import clsx from 'clsx'
import Button from 'components/Button'
import Countdown from 'components/Countdown'
import s from './ProductCard.module.scss'
import ActionCard from './component/ActionCard'
import IconETH from './component/IconETH'

interface CardProps extends AntdCardProps {
  button: ReactNode
  typeAction?: 'simple' | 'more'
  subcontent?: ReactNode
  noteContent?: ReactNode
  typeCard?: 'buy' | 'auction'
  sizeCard?: 'sm' | 'md' | 'lg'
}
const ProductCard: FC<CardProps> = ({ className, button, typeAction, sizeCard, size, noteContent, ...rest }) => (
  <div className={clsx({ [s.space]: sizeCard === 'md' })}>
    <AntdCard
      className={clsx({
        [s.root]: true,
        [className || '']: true,
        [s.sm]: sizeCard === 'sm',
        [s.md]: sizeCard === 'md',
      })}
      cover={
        <img
          className={clsx({
            [s.img]: true,
            [s.imgSm]: sizeCard === 'sm',
            [s.imgMd]: sizeCard === 'md',
          })}
          src="https://s3.coinmarketcap.com/generated/nft/collections/raritysniper-mutant-ape-yacht-club.png"
          alt="#"
        />
      }
      {...rest}
    >
      {sizeCard === 'md' ? (
        <div
          className={clsx({
            [s.reverseForMd]: sizeCard === 'md',
          })}
        >
          <Space className={s.content}>
            <div className={s.noteContent}>{noteContent}</div>
            <ActionCard type={typeAction} />
          </Space>
          <div className={s.name}>Morganitho Arts</div>
        </div>
      ) : (
        <div
          className={clsx({
            [s.reverseForSm]: sizeCard === 'sm',
          })}
        >
          <div className={s.noteContent}>{noteContent}</div>
          <div className={s.content}>
            <div className={s.name}>Morganitho</div>
            <ActionCard type={typeAction} />
          </div>
        </div>
      )}
      <Space className={s.creatorprice}>
        <Space>
          <Avatar size={20} />
          <div className={s.creatorname}>@James_alex01</div>
        </Space>
        <Space>
          <IconETH typeIconETH="ETHBlue" />
          <div className={s.price}>0.16ETH</div>
        </Space>
      </Space>

      <Space className={s.btncountdown}>
        <Button className={s.btn} type="primary">
          PlaceBid
        </Button>
        <div
          className={clsx({
            [s.countdown]: true,
            [s.lg]: sizeCard === 'lg',
          })}
        >
          <Countdown />
        </div>
      </Space>
    </AntdCard>
  </div>
)
export default ProductCard
