import React, { FC } from 'react'
import clsx from 'clsx'
import { Space } from 'antd'
import s from './ActionCard.module.scss'

interface ActionCardProps {
  type?: 'simple' | 'more'
}

const ActionCard: FC<ActionCardProps> = ({ type }) => (
  <Space>
    <div>
      <button className={s.icon} type="button">
        <img src="./img/icon/icon-likes.svg" alt="#" />
      </button>
    </div>
    <Space className={clsx({
      [s.more]: true,
      [s.visible]: type === 'more',
    })}
    >
      <Space>
        <div>
          <button className={s.icon} type="button">
            <img src="./img/icon/icon-share.svg" alt="#" />
          </button>
        </div>
        <div>
          <button className={s.icon} type="button">
            <img src="./img/icon/icon-star.svg" alt="#" />
          </button>
        </div>
      </Space>
    </Space>
  </Space>
)

export default ActionCard
