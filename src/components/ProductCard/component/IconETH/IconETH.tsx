import { Space } from 'antd'
import { FC } from 'react'
import s from './IconETH.module.scss'

interface IconETHProps {
  typeIconETH?: 'ETHBlue' | 'ETHBlack'
}
const IconETH: FC<IconETHProps> = ({ typeIconETH }) => (
  <div>
    {typeIconETH === 'ETHBlack' ? (
      <Space>
        <img src="./img/icon/icon-eth.svg" alt="#" />
        <p className={s.text}>New Bid: </p>
      </Space>
    ) : (
      <Space>
        <img src="./img/icon/icon-eth-fillter.svg" alt="#" />
        <p className={s.text}>Price:</p>
      </Space>
    )}
  </div>
)
export default IconETH
