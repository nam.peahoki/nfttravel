import { Space } from 'antd'
import Button from 'components/Button'
import ProductCard from 'components/ProductCard'

import React, { useState } from 'react'

const HomePage = () => {
  const textplacebid = 'Place Bid'
  const textexploreitems = 'Explore Items'
  const [placebidbtn, setPlaceBidBtn] = useState(textplacebid)

  return (
    <div>
      <Button
        type="primary"
        onMouseOver={() => setPlaceBidBtn('Buy Now')}
        onMouseLeave={() => setPlaceBidBtn(textplacebid)}
      >
        {placebidbtn}
      </Button>
      <br />
      <br />

      <Button type="text">ExploreItems</Button>
      <br />
      <br />
      <Space>
        <ProductCard button={<Button>hello</Button>} typeAction="simple" sizeCard="lg" />
        <ProductCard
          button={<Button>hello</Button>}
          typeAction="more"
          sizeCard="md"
          noteContent="Highest bid 0.24ETH"
        />
        <ProductCard
          button={<Button>hello</Button>}
          typeAction="simple"
          sizeCard="sm"
          noteContent="Highest bid 0.24ETH"
          size="small"
        />
      </Space>
    </div>
  )
}
export default HomePage
