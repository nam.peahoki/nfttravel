import { createContext, ReactElement, useEffect, useState } from 'react'
import useAuth from 'hooks/auth'
import { IUserProfile } from 'interface/auth'
import { TOKEN_STORAGE_KEY } from 'constants/storage-keys'

interface IAuthContext {
  session: IUserProfile | undefined
  loading: boolean
  setSession: (session: IUserProfile | undefined) => void
}

const AuthContext = createContext<IAuthContext>({
  session: undefined,
  loading: true,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setSession: (session: any) => {},
})

const AuthContextProvider = ({ children }: { children: ReactElement }) => {
  const [loading, setLoading] = useState<boolean>(true)
  const [currentSession, setCurrentSession] = useState<IUserProfile | undefined>(undefined)
  const { profileData: sessionData, error, getUserProfile } = useAuth()
  const storageToken = typeof window !== 'undefined' ? localStorage?.getItem(TOKEN_STORAGE_KEY) : ''

  useEffect(() => {
    if (!storageToken) {
      setLoading(false)
      return
    }

    getUserProfile()
  }, [storageToken])

  useEffect(() => {
    if (error !== undefined) {
      setCurrentSession(undefined)
      setLoading(false)
    } else if (sessionData !== undefined) {
      const fullSessionData = {
        ...sessionData,
        token: localStorage.getItem(TOKEN_STORAGE_KEY),
      } as IUserProfile
      setCurrentSession(fullSessionData)
      setLoading(false)
    }
  }, [sessionData, error])

  return (
    <AuthContext.Provider
      value={{
        session: currentSession,
        loading,
        setSession: setCurrentSession,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export { AuthContext, AuthContextProvider }
