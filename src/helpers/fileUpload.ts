import { RcFile } from 'antd/lib/upload/interface'

export const getBase64 = (img: RcFile, callback: (p: string) => void) => {
  const reader = new FileReader()

  reader.addEventListener('load', () => callback(reader.result as string))
  reader.readAsDataURL(img)
}
