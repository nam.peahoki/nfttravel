export const formatAddress = (address: string) => {
  let a = address
  a = address.toUpperCase()
  const head = address.substring(0, 4)
  const tail = address.substring(address.length - 5, address.length)
  return `${head}...${tail}`
}
