export const numberWithCommas = (val: string | number, precision = 0) => {
  if (!val) return 0
  return parseFloat(parseFloat(String(val)).toFixed(precision))
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}
