// @ts-nocheck
import React, { FC, useContext } from 'react'
import { useRouter } from 'next/router'
import { AuthContext } from 'context/auth'
import * as ROUTES from 'shell/route'

const withAuthHOC: any = (Component: FC) => {
  const WithAuthHOC = (props: any) => {
    const { session, loading } = useContext(AuthContext)
    const router = useRouter()
    const currentRoute = router.pathname
    const AUTH_PAGES = [ROUTES.SIGN_UP_PAGE, ROUTES.LOGIN_PAGE, ROUTES.VERIFY_ACCOUNT_PAGE]
    const GUARD_PAGES = [ROUTES.PROFILE_PAGE]

    if (loading) {
      return null
    }

    if (!session) {
      if (GUARD_PAGES.includes(currentRoute)) {
        router.replace(ROUTES.LOGIN_PAGE)
        return null
      }
    } else if (AUTH_PAGES.includes(currentRoute)) {
      router.replace(ROUTES.HOME_PAGE)
      return null
    }

    return <Component {...props} />
  }

  return WithAuthHOC
}

export default withAuthHOC
