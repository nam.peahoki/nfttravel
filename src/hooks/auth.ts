import { ISignUp, IUserProfile, IUpdatePasswordReq, IResetPasswordReq } from 'interface/auth'
import { useState } from 'react'
import { IBaseResponse } from 'interface/base'
import axios from 'shell/axios'
import { useNotification } from 'hooks/notification'

const useAuth = () => {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<any>(undefined)
  const { showNotification } = useNotification()

  /* ----------------------- register request ------------------------------ */
  const [registerData, setRegisterData] = useState<boolean | undefined>(undefined)
  const registerAccount = async (account: ISignUp) => {
    try {
      setLoading(true)
      const { data: res } = await axios.post<IBaseResponse<boolean>>('/user/register', account)
      if (res.data) {
        setRegisterData(res.data)
      }
    } catch (e) {
      setError(e)
    } finally {
      setLoading(false)
    }
  }

  /* ----------------------- verify request ------------------------------ */
  const [verifyData, setVerifyData] = useState<boolean>(false)
  const verifyAccount = async (verfiyToken: string) => {
    try {
      const res = await axios.get('/user/register/verify', {
        params: {
          token: verfiyToken,
        },
      })

      if (res.data) {
        setVerifyData(res.data)
      }
    } catch (e) {
      setError(e)
    } finally {
      setLoading(false)
    }
  }

  /* ----------------------- user profile request ------------------------------ */
  const [profileData, setProfileData] = useState<IUserProfile | undefined>(undefined)
  const getUserProfile = async () => {
    try {
      const { data: res } = await axios.get<IBaseResponse<IUserProfile>>('/user/auth/me')
      if (res.data) {
        setProfileData(res.data)
      }
    } catch (e) {
      setError(e)
    }
  }

  /* ----------------------- update password ------------------------------ */
  const [updatePasswordRes, setUpdatePasswordRes] = useState<boolean | undefined>(undefined)
  const updatePassword = async (req: IUpdatePasswordReq) => {
    try {
      setLoading(true)
      const { data: res } = await axios.put<IBaseResponse<boolean>>('/user/me/password', req)
      if (res.data) {
        setUpdatePasswordRes(res.data)
      }
    } catch (e) {
      setError(e)
    } finally {
      setLoading(false)
    }
  }

  /* ----------------------- forgot password ------------------------------ */
  const [sendForgotPasswordEmailRes, setSendForgotPasswordEmailRes] = useState<boolean | undefined>(undefined)
  const sendForgotPasswordEmail = async (email: string) => {
    try {
      setLoading(true)
      const { data: res } = await axios.put<IBaseResponse<boolean>>('/user/me/forgot-password', { email })
      if (res.data) {
        setSendForgotPasswordEmailRes(res.data)
        showNotification({
          type: 'success',
          message: 'Email sent',
          description: 'Please check your email to reset your password',
        })
      }
    } catch (e) {
      setError(e)
      showNotification({
        description: 'Have occur error. Please try again',
      })
    } finally {
      setLoading(false)
    }
  }

  /* ----------------------- reset password ------------------------------ */
  const [resetPasswordRes, setResetPasswordRes] = useState<boolean | undefined>(undefined)
  const resetPassword = async (req: IResetPasswordReq) => {
    try {
      setLoading(true)
      const { data: res } = await axios.put<IBaseResponse<boolean>>('/user/me/reset-password', req)
      if (res.data) {
        setResetPasswordRes(res.data)
        showNotification({
          type: 'success',
          message: 'Success',
          description: 'Password has been reset. Please login with new password',
        })
      }
    } catch (e) {
      setError(e)
      showNotification({
        description: 'Have occur error. Please try again',
      })
    } finally {
      setLoading(false)
    }
  }

  return {
    loading,
    error,

    registerAccount,
    registerData,

    verifyData,
    verifyAccount,

    profileData,
    getUserProfile,

    updatePassword,
    updatePasswordRes,

    sendForgotPasswordEmail,
    sendForgotPasswordEmailRes,

    resetPassword,
    resetPasswordRes,
  }
}

export default useAuth
