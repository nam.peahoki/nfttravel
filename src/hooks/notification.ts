import { notification } from 'antd'
import { useCallback, useEffect } from 'react'

export const useNotification = () => {
  const showNotification = useCallback(
    ({ type = 'error', placement = 'bottomRight', duration = 0, message, description }) => {
      notification.destroy()

      notification.open({
        placement,
        duration,
        message,
        description,
        type,
      })
    },
    [],
  )

  useEffect(
    () => () => {
      notification.destroy()
    },
    [],
  )

  return {
    showNotification,
  }
}
