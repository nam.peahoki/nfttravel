export interface ISignIn {
  email: string
  password: string
}

export interface ISignUp extends ISignIn {
  name?: string
}

export interface IUserProfile {
  _id: string
  name: string
  token: string
}

export interface IUpdatePasswordReq {
  oldPassword: string
  newPassword: string
}

export interface IResetPasswordReq {
  token: string
  newPassword: string
}
