interface IError {
  errorType: number
  message: string
}

export interface IBaseResponse<T = any> {
  success: boolean
  data: T
  error?: IError
}

export interface ISkeletonProps {
  loading: boolean
  count?: number
}
