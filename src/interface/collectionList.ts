export interface ICollectionCard {
  _id: string
  userId: string
  path: string
  contentType: string
  title: string
  category: string[]
  price: number
  description: string
  status: string
  createdAt: number
}
