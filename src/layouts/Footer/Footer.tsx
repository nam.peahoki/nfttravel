import { Input } from 'antd'
import React, { FC } from 'react'
import s from './Footer.module.scss'

const Footer: FC = () => (
  <footer className={s.root}>
    <div className={s.footer}>
      <div className={s.titlelink}>
        <div className={s.left}>
          <div className={s.logo}>
            <p>Makinft</p>
          </div>
          <div className={s.title}>
            <p>Subscrible Newsletter</p>
          </div>
          <div>
            <Input className={s.input} placeholder="Email address" />
          </div>
          <div className={s.social}>
            <img src="/img/icon/icon-social-in.svg" alt="#" />
            <img src="/img/icon/icon-social-intergram.svg" alt="#" />
            <img src="/img/icon/icon-social-youtube.svg" alt="#" />
            <img src="/img/icon/icon-social-facebook.svg" alt="#" />
          </div>
        </div>
        <div className={s.right}>
          <div>
            <p className={s.label}>My Account</p>
            <ul>
              <li className={s.sublabel}>Create Dashboard</li>
              <li className={s.sublabel}>Wallet</li>
              <li className={s.sublabel}>Create Team</li>
              <li className={s.sublabel}>MyNFTs</li>
            </ul>
          </div>
          <div>
            <p className={s.label}>UseFul Links</p>
            <ul>
              <li className={s.sublabel}>All NFTs</li>
              <li className={s.sublabel}>How it works</li>
              <li className={s.sublabel}>Create</li>
              <li className={s.sublabel}>Explore</li>
              <li className={s.sublabel}>Collection</li>
            </ul>
          </div>
          <div>
            <p className={s.label}>Contact Us</p>
            <ul>
              <li className={s.sublabel}>A: 60 Truong Son,Phuong2, Quan Tan Binh</li>
              <li className={s.sublabel}>P: +84 123456789</li>
              <li className={s.sublabel}>G: Peahoki@gmail.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
