import { Menu } from 'antd'
import React from 'react'
import s from './Header.module.scss'

const Header = () => (
  <header className={s.root}>
    <div className={s.logo}>Makinft</div>
    <div className={s.menubutton}>
      <Menu mode="horizontal" className={s.menu}>
        <Menu.Item>Home </Menu.Item>
        <Menu.Item>Explore </Menu.Item>
        <Menu.Item>Activity </Menu.Item>
        <Menu.Item>Pages </Menu.Item>
        <Menu.Item>Authors </Menu.Item>
        <Menu.Item>Contact</Menu.Item>
      </Menu>
      <button type="button" className={s.btn}>
        <img src="/img/icon/icon-wallet.svg" alt="#" />
        Connect Walllet
      </button>
    </div>
  </header>
)

export default Header
