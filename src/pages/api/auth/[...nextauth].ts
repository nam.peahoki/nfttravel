/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */

import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
import getConfig from 'next/config'

export default NextAuth({
  providers: [
    Providers.Credentials({
      name: 'Credentials',
      authorize: async (credentials) => {
        const res = await fetch(`${getConfig().publicRuntimeConfig.API_ENDPOINT}/user/auth/login`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(credentials),
        })

        const token = await res.json()

        if (token.data) {
          return token.data
        }
        return null
      },
    }),
  ],

  callbacks: {
    async jwt(token, user, account) {
      // Add access_token to the token right after signin
      if (account?.accessToken) {
        token.accessToken = account.accessToken
        token.type = account.type
      }

      if (user && token && account?.type === 'credentials') {
        token = {
          ...token,
          token: user,
          type: account.type,
        }
      }

      return token
    },

    async signIn() {
      return true
    },

    async session(_, user) {
      if (user.token) {
        const profileRes = await fetch(`${getConfig().publicRuntimeConfig.API_ENDPOINT}/user/auth/me`, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user.token}`,
          },
        })

        const { data: profileUser } = await profileRes.json()

        user = {
          ...user,
          name: profileUser.name,
          picture: '/img/avatar-default.png',
          type: user.type,
          ...profileUser,
        }
      }

      return user
    },
  },

  pages: {
    signIn: '/auth/sign-in',
  },

  // Enable debug messages in the console if you are having problems
  debug: process.env.NODE_ENV === 'development',
})
