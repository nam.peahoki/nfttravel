/* eslint-disable indent */
import { GetServerSideProps } from 'next'

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const tab = query.tab ? query.tab : 'popular'

  return {
    props: {
      tab,
    },
  }
}

export { default } from 'containers/HomePage'
